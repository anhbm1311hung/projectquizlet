USE [master]
GO
/****** Object:  Database [Quizlett]    Script Date: 3/22/2023 4:15:27 AM ******/
CREATE DATABASE [Quizlett]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Quizlett', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Quizlett.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Quizlett_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Quizlett_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Quizlett] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Quizlett].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Quizlett] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Quizlett] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Quizlett] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Quizlett] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Quizlett] SET ARITHABORT OFF 
GO
ALTER DATABASE [Quizlett] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Quizlett] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Quizlett] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Quizlett] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Quizlett] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Quizlett] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Quizlett] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Quizlett] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Quizlett] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Quizlett] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Quizlett] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Quizlett] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Quizlett] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Quizlett] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Quizlett] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Quizlett] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Quizlett] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Quizlett] SET RECOVERY FULL 
GO
ALTER DATABASE [Quizlett] SET  MULTI_USER 
GO
ALTER DATABASE [Quizlett] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Quizlett] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Quizlett] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Quizlett] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Quizlett] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Quizlett] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'Quizlett', N'ON'
GO
ALTER DATABASE [Quizlett] SET QUERY_STORE = OFF
GO
USE [Quizlett]
GO
/****** Object:  Table [dbo].[Answers]    Script Date: 3/22/2023 4:15:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Answers](
	[AnswerID] [int] IDENTITY(1,1) NOT NULL,
	[Answer] [varchar](max) NOT NULL,
	[ExID] [int] NOT NULL,
 CONSTRAINT [PK_Answers] PRIMARY KEY CLUSTERED 
(
	[AnswerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Book]    Script Date: 3/22/2023 4:15:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Book](
	[ISBN] [nvarchar](20) NOT NULL,
	[Title] [varchar](max) NOT NULL,
	[Edition] [varchar](max) NOT NULL,
	[Authors] [varchar](max) NOT NULL,
	[Image] [varchar](max) NOT NULL,
	[NumOfAnswers] [varchar](max) NOT NULL,
	[CateID] [int] NOT NULL,
 CONSTRAINT [PK_Book] PRIMARY KEY CLUSTERED 
(
	[ISBN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Card]    Script Date: 3/22/2023 4:15:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Card](
	[cardId] [int] IDENTITY(1,1) NOT NULL,
	[term] [nvarchar](max) NULL,
	[definition] [nvarchar](max) NULL,
	[studySetId] [int] NULL,
 CONSTRAINT [PK_Card] PRIMARY KEY CLUSTERED 
(
	[cardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 3/22/2023 4:15:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CateID] [int] IDENTITY(1,1) NOT NULL,
	[CateName] [varchar](max) NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[CateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Chapter]    Script Date: 3/22/2023 4:15:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Chapter](
	[ChapterID] [int] NOT NULL,
	[ChapterName] [varchar](max) NOT NULL,
	[ISBN] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_Chapter] PRIMARY KEY CLUSTERED 
(
	[ChapterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Class]    Script Date: 3/22/2023 4:15:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Class](
	[classId] [int] IDENTITY(1,1) NOT NULL,
	[className] [nvarchar](max) NULL,
	[description] [nvarchar](max) NULL,
	[isInvite] [bit] NULL,
	[inviteCode] [nchar](10) NULL,
	[isEdit] [bit] NULL,
	[schoolName] [nvarchar](max) NULL,
	[userId] [int] NULL,
 CONSTRAINT [PK_Class] PRIMARY KEY CLUSTERED 
(
	[classId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Exercises]    Script Date: 3/22/2023 4:15:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Exercises](
	[ExID] [int] IDENTITY(1,1) NOT NULL,
	[ExName] [varchar](max) NOT NULL,
	[Answers] [varchar](max) NOT NULL,
	[PageID] [int] NOT NULL,
 CONSTRAINT [PK_Exercises] PRIMARY KEY CLUSTERED 
(
	[ExID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FlashCards]    Script Date: 3/22/2023 4:15:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FlashCards](
	[userId] [int] NOT NULL,
	[cardId] [int] NOT NULL,
	[studySetId] [int] NOT NULL,
	[flashCardId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_FlashCards] PRIMARY KEY CLUSTERED 
(
	[flashCardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Folder]    Script Date: 3/22/2023 4:15:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Folder](
	[folderId] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](50) NULL,
	[description] [nvarchar](max) NULL,
	[userId] [int] NULL,
	[isShare] [bit] NULL,
 CONSTRAINT [PK_Folder] PRIMARY KEY CLUSTERED 
(
	[folderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LearnCards]    Script Date: 3/22/2023 4:15:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LearnCards](
	[learnId] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NOT NULL,
	[cardId] [int] NOT NULL,
	[studySetId] [int] NOT NULL,
 CONSTRAINT [PK_LearnCards] PRIMARY KEY CLUSTERED 
(
	[learnId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ListClass]    Script Date: 3/22/2023 4:15:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListClass](
	[listClassId] [int] IDENTITY(1,1) NOT NULL,
	[classId] [int] NOT NULL,
	[folderId] [int] NOT NULL,
 CONSTRAINT [PK_ListClass] PRIMARY KEY CLUSTERED 
(
	[listClassId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ListEditId]    Script Date: 3/22/2023 4:15:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListEditId](
	[studySetId] [int] NULL,
	[userId] [int] NULL,
	[listEditId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_ListEditId] PRIMARY KEY CLUSTERED 
(
	[listEditId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ListFolder]    Script Date: 3/22/2023 4:15:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListFolder](
	[studySetId] [int] NULL,
	[folderId] [int] NULL,
	[ListFolderId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_ListFolder] PRIMARY KEY CLUSTERED 
(
	[ListFolderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ListMember]    Script Date: 3/22/2023 4:15:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListMember](
	[classId] [int] NULL,
	[userId] [int] NULL,
	[listMemberId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_ListMember] PRIMARY KEY CLUSTERED 
(
	[listMemberId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ListStudySet]    Script Date: 3/22/2023 4:15:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListStudySet](
	[classId] [int] NULL,
	[studySetId] [int] NULL,
	[ListStudySetId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_ListStudySet] PRIMARY KEY CLUSTERED 
(
	[ListStudySetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Page]    Script Date: 3/22/2023 4:15:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Page](
	[PageID] [int] IDENTITY(1,1) NOT NULL,
	[PageName] [varchar](max) NOT NULL,
	[ChapterID] [int] NOT NULL,
 CONSTRAINT [PK_Page] PRIMARY KEY CLUSTERED 
(
	[PageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudySet]    Script Date: 3/22/2023 4:15:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudySet](
	[studySetId] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](max) NULL,
	[description] [nvarchar](max) NULL,
	[isShare] [bit] NULL,
	[userId] [int] NULL,
 CONSTRAINT [PK_StudySet] PRIMARY KEY CLUSTERED 
(
	[studySetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 3/22/2023 4:15:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[name] [nvarchar](50) NULL,
	[password] [nvarchar](50) NULL,
	[gmail] [nvarchar](50) NULL,
	[isActive] [bit] NULL,
	[avatar] [nvarchar](max) NULL,
	[userId] [int] IDENTITY(1,1) NOT NULL,
	[language] [nvarchar](50) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[Book] ([ISBN], [Title], [Edition], [Authors], [Image], [NumOfAnswers], [CateID]) VALUES (N'9780073511177', N'Chemistry: The Molecular Nature of Matter and Change', N'7th Edition', N'Bruce Edward Bursten, Catherine J. Murphy, H. Eugene Lemay, Matthew E. Stoltzfus, Patrick Woodward, Theodore E. Brown', N'https://d2nchlq0f2u6vy.cloudfront.net/cache/93/e0/93e0bb4901e3a1e6d9d963928d1d1bc9.jpg', N'3.711 l?i gi?i', 1)
INSERT [dbo].[Book] ([ISBN], [Title], [Edition], [Authors], [Image], [NumOfAnswers], [CateID]) VALUES (N'9780134414232', N'Chemistry: The Central Science', N'14th Edition', N'Bruce Edward Bursten, Catherine J. Murphy, H. Eugene Lemay, Matthew E. Stoltzfus, Patrick Woodward, Theodore E. Brown', N'https://d2nchlq0f2u6vy.cloudfront.net/cache/f0/5b/f05b21063194c1b8a8c430c213bdce16.jpg', N'7.741 l?i gi?i', 1)
INSERT [dbo].[Book] ([ISBN], [Title], [Edition], [Authors], [Image], [NumOfAnswers], [CateID]) VALUES (N'9780198769866', N'Atkins'' Physical Chemistry', N'11th Edition', N'James Keeler, Julio de Paula, Peter Atkins', N'https://d2nchlq0f2u6vy.cloudfront.net/cache/08/37/083703225d5b4f62381c6fffd012e7df.jpg', N'1.691 l?i gi?i', 1)
INSERT [dbo].[Book] ([ISBN], [Title], [Edition], [Authors], [Image], [NumOfAnswers], [CateID]) VALUES (N'9780470647691', N'Calculus: Early Transcendentals', N'10th Edition', N'Howard Anton, Irl C. Bivens, Stephen Davis', N'https://d2nchlq0f2u6vy.cloudfront.net/cache/86/91/869127732f2fbf1b078bdc23328a70a9.jpg', N'10.485 l?i gi?i', 2)
INSERT [dbo].[Book] ([ISBN], [Title], [Edition], [Authors], [Image], [NumOfAnswers], [CateID]) VALUES (N'9780547586632', N'Modern Chemistry', N'1st Edition', N'Jerry L. Sarquis, Mickey Sarquis', N'https://d2nchlq0f2u6vy.cloudfront.net/cache/81/6d/816de1f9efab9b705c98ad70792c0765.jpg', N'2.184 l?i gi?i', 1)
INSERT [dbo].[Book] ([ISBN], [Title], [Edition], [Authors], [Image], [NumOfAnswers], [CateID]) VALUES (N'9781119316152', N'Organic Chemistry', N'3rd Edition', N'David Klein', N'https://d2nchlq0f2u6vy.cloudfront.net/cache/62/2f/622f88fbfd4502941b87f4a7e4cdf69b.jpg', N'3.098 l?i gi?i', 1)
INSERT [dbo].[Book] ([ISBN], [Title], [Edition], [Authors], [Image], [NumOfAnswers], [CateID]) VALUES (N'9781305580350', N'Organic Chemistry', N'8th Edition', N'Brent L. Iverson, Christopher S. Foote, Eric Anslyn, William H. Brown', N'https://d2nchlq0f2u6vy.cloudfront.net/cache/8d/3f/8d3fcdc812de6ae2c0d2f91fec1b4a84.jpg', N'1.464 l?i gi?i', 1)
GO
SET IDENTITY_INSERT [dbo].[Card] ON 

INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4018, N'distinguished professor', N'giáo sư ưu tú', 2011)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4019, N'maxillofacial', N'hàm mặt', 2011)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4020, N'radiology', N'phóng xạ học', 2011)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4021, N'modality', N'phương thức, thể thức, dạng', 2011)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4022, N'access to', N'tiếp cận', 2011)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4023, N'Dentists today have ready access to a variety of excellent imaging modalities to assist in the care of their patients', N'Các nha sĩ ngày nay đã sẵn sàng tiếp cận với nhiều loại phương thức hình ảnh tuyệt vời để hỗ trợ chăm sóc bệnh nhân của họ', 2011)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4024, N'panoramic', N'panoramic', 2011)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4025, N'cone-beam computed tomographic (CBCT)', N'chụp cắt lớp chùm tia X hình nón', 2011)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4026, N'multidetector computed tomographic (CT) scanners', N'multidetector computed tomographic (CT) scanners', 2011)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4027, N'multidetector computed tomographic (CT) scanners', N'máy quét hình ảnh cộng hưởng từ', 2011)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4028, N'paranasal sinuses', N'xoang cạnh mũi (xoang trán, xoang hàm trên, xoang bướm)', 2012)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4029, N'temporomandibular joints', N'khớp thái dương - hàm', 2012)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4030, N'Emphasis is placed on the role of understanding the underlying mechanisms of various disease processes to enhance the interpretation of abnormalities as they can appear in various imaging modalities', N'Nhấn mạnh được đặt lên vai trò ở chỗ hiểu được những cơ chế cơ bản của các quá trình bệnh khác nhau và nâng cao việc đọc phim về những bất thường vì chúng có thể hiện lên những phương thức hình ảnh khác nhau', 2012)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4031, N'diagnostician', N'bác sĩ chẩn đoán', 2012)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4032, N'To be a good diagnostician, it is helpful to be curious, observant, systematic, and thorough', N'để trở thành 1 bác sĩ chẩn đoán giỏi, rất hữu ích khi mà bạn hay tò mò, quan sát, có hệ thống và kĩ lưỡng', 2012)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4033, N'conducting the physical examination', N'tiến hành kiểm tra thể chất', 2012)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4034, N'Successful treatment critically depends on accurate diagnosis', N'điều trị thành công phụ thuộc quan trọng vào chẩn đoán chính xác', 2012)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4035, N'well versed in', N' thành thạo', 2012)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4036, N'In general, dentists interpret most of the images they prescribe and produce. This responsibility places a special burden on dentists to be well versed in the means of acquiring optimal images as well as in their interpretation', N'nói chung, nha sĩ đọc hầu hết hình ảnh họ chỉ định và chụp. Điều này đặt lên gánh nặng đặc biệt lên người nha sĩ phải thành thạo các phương tiện để có được hình ảnh tối ưu cũng như cách giải thích của họ', 2012)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4037, N'three-dimensional representations', N'biểu diễn 3 chiều', 2012)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4038, N'きょねん', N'last year', 2013)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4039, N'ことし', N'this year', 2013)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4040, N'らいねん', N'next year', 2013)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4041, N'ぜんしゅう', N'last week', 2013)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4042, N'こんしゅう', N'this week', 2013)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4043, N'らいしゅう　（also じしゅう)', N'next week', 2013)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4044, N'もういちどいってください', N'Please say it again', 2013)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4045, N'かぞく', N'family', 2013)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4046, N'ざっし', N'magazine', 2013)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4047, N'娘 (むすめ)', N' daughter', 2013)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4048, N'へん', N'weird', 2014)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4049, N'いつ', N'when', 2014)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4050, N'ゆっくり おっしゃって ください', N'Please speak slowly', 2014)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4051, N'しぬ', N'to die', 2014)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4052, N'よぶ', N'to call (one''s name); to invite', 2014)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4053, N'いう', N'to say', 2014)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4054, N'えいがかん', N'movie theater', 2014)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4055, N'おきる', N'to wake up', 2014)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4056, N'ねる', N'to sleep; to go to sleep', 2014)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (4057, N'あける', N'to open', 2014)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (5038, N'aegawg', N'aegawg', 3021)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (5039, N'aewg', N'aegaegwag', 3021)
INSERT [dbo].[Card] ([cardId], [term], [definition], [studySetId]) VALUES (8045, N'aeg', N'aewga', 2011)
SET IDENTITY_INSERT [dbo].[Card] OFF
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([CateID], [CateName]) VALUES (1, N'Hóa h?c')
INSERT [dbo].[Category] ([CateID], [CateName]) VALUES (2, N'Gi?i tích')
INSERT [dbo].[Category] ([CateID], [CateName]) VALUES (3, N'K? Thu?t')
INSERT [dbo].[Category] ([CateID], [CateName]) VALUES (4, N'Ð?i s? tuy?n tính')
INSERT [dbo].[Category] ([CateID], [CateName]) VALUES (5, N'V?t lý')
INSERT [dbo].[Category] ([CateID], [CateName]) VALUES (6, N'Sinh h?c')
INSERT [dbo].[Category] ([CateID], [CateName]) VALUES (7, N'Ngôn ng?')
INSERT [dbo].[Category] ([CateID], [CateName]) VALUES (8, N'Kinh doanh')
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
INSERT [dbo].[Chapter] ([ChapterID], [ChapterName], [ISBN]) VALUES (1, N'Chuong 1:   Introduction: Matter, Energy, and Measurement', N'9780134414232')
INSERT [dbo].[Chapter] ([ChapterID], [ChapterName], [ISBN]) VALUES (2, N'Chuong 2:   Atoms, Molecules, and Ions', N'9780134414232')
INSERT [dbo].[Chapter] ([ChapterID], [ChapterName], [ISBN]) VALUES (3, N'Chuong 3:   Chemical Reactions and Reaction Stoichiometry', N'9780134414232')
INSERT [dbo].[Chapter] ([ChapterID], [ChapterName], [ISBN]) VALUES (25, N'Chuong 1:    Keys to the Study of Chemistry', N'9780073511177')
GO
SET IDENTITY_INSERT [dbo].[Class] ON 

INSERT [dbo].[Class] ([classId], [className], [description], [isInvite], [inviteCode], [isEdit], [schoolName], [userId]) VALUES (1, N'aegaewga', N'aewga', 0, N'          ', 0, N'', 1)
INSERT [dbo].[Class] ([classId], [className], [description], [isInvite], [inviteCode], [isEdit], [schoolName], [userId]) VALUES (2, N'gsdsgsgs', N'', 1, N'          ', 1, N'', 1)
INSERT [dbo].[Class] ([classId], [className], [description], [isInvite], [inviteCode], [isEdit], [schoolName], [userId]) VALUES (3, N'jghkjlm', N'', 0, N'          ', 0, N'', 1)
INSERT [dbo].[Class] ([classId], [className], [description], [isInvite], [inviteCode], [isEdit], [schoolName], [userId]) VALUES (1002, N'viet anh', N'1234565', 0, N'          ', 0, N'', 1)
SET IDENTITY_INSERT [dbo].[Class] OFF
GO
SET IDENTITY_INSERT [dbo].[Exercises] ON 

INSERT [dbo].[Exercises] ([ExID], [ExName], [Answers], [PageID]) VALUES (1, N'Bài tap 1', N'1', 1)
INSERT [dbo].[Exercises] ([ExID], [ExName], [Answers], [PageID]) VALUES (2, N'Bài tap 2', N'1', 1)
INSERT [dbo].[Exercises] ([ExID], [ExName], [Answers], [PageID]) VALUES (3, N'Bài tap 1', N'1', 2)
INSERT [dbo].[Exercises] ([ExID], [ExName], [Answers], [PageID]) VALUES (4, N'Bài tap 2a', N'1', 2)
INSERT [dbo].[Exercises] ([ExID], [ExName], [Answers], [PageID]) VALUES (5, N'Bài tap 2b', N'1', 2)
INSERT [dbo].[Exercises] ([ExID], [ExName], [Answers], [PageID]) VALUES (7, N'Bài tap 1', N'1', 3)
INSERT [dbo].[Exercises] ([ExID], [ExName], [Answers], [PageID]) VALUES (8, N'Bài tap 2', N'1', 3)
SET IDENTITY_INSERT [dbo].[Exercises] OFF
GO
SET IDENTITY_INSERT [dbo].[FlashCards] ON 

INSERT [dbo].[FlashCards] ([userId], [cardId], [studySetId], [flashCardId]) VALUES (1, 4028, 2012, 5022)
INSERT [dbo].[FlashCards] ([userId], [cardId], [studySetId], [flashCardId]) VALUES (1, 4029, 2012, 5023)
INSERT [dbo].[FlashCards] ([userId], [cardId], [studySetId], [flashCardId]) VALUES (1, 4030, 2012, 5024)
INSERT [dbo].[FlashCards] ([userId], [cardId], [studySetId], [flashCardId]) VALUES (1, 4031, 2012, 5025)
INSERT [dbo].[FlashCards] ([userId], [cardId], [studySetId], [flashCardId]) VALUES (1, 4032, 2012, 5026)
INSERT [dbo].[FlashCards] ([userId], [cardId], [studySetId], [flashCardId]) VALUES (1, 4033, 2012, 5027)
INSERT [dbo].[FlashCards] ([userId], [cardId], [studySetId], [flashCardId]) VALUES (1, 4034, 2012, 5028)
INSERT [dbo].[FlashCards] ([userId], [cardId], [studySetId], [flashCardId]) VALUES (1, 4038, 2013, 5029)
INSERT [dbo].[FlashCards] ([userId], [cardId], [studySetId], [flashCardId]) VALUES (1, 4039, 2013, 5030)
INSERT [dbo].[FlashCards] ([userId], [cardId], [studySetId], [flashCardId]) VALUES (1, 4018, 2011, 5031)
INSERT [dbo].[FlashCards] ([userId], [cardId], [studySetId], [flashCardId]) VALUES (1, 4019, 2011, 5032)
INSERT [dbo].[FlashCards] ([userId], [cardId], [studySetId], [flashCardId]) VALUES (1, 4020, 2011, 5033)
SET IDENTITY_INSERT [dbo].[FlashCards] OFF
GO
SET IDENTITY_INSERT [dbo].[Folder] ON 

INSERT [dbo].[Folder] ([folderId], [title], [description], [userId], [isShare]) VALUES (1003, N'test', N'awgeag', 1, 0)
INSERT [dbo].[Folder] ([folderId], [title], [description], [userId], [isShare]) VALUES (1004, N'english1', N'awegag', 1, 0)
INSERT [dbo].[Folder] ([folderId], [title], [description], [userId], [isShare]) VALUES (1005, N'Vocabulary', N'', 1, 0)
INSERT [dbo].[Folder] ([folderId], [title], [description], [userId], [isShare]) VALUES (1006, N'Vocabulary Japanese', N'', 1, 0)
INSERT [dbo].[Folder] ([folderId], [title], [description], [userId], [isShare]) VALUES (2002, N'English', N'', 1, 0)
SET IDENTITY_INSERT [dbo].[Folder] OFF
GO
SET IDENTITY_INSERT [dbo].[LearnCards] ON 

INSERT [dbo].[LearnCards] ([learnId], [userId], [cardId], [studySetId]) VALUES (1073, 1, 4018, 2011)
INSERT [dbo].[LearnCards] ([learnId], [userId], [cardId], [studySetId]) VALUES (1074, 1, 4019, 2011)
INSERT [dbo].[LearnCards] ([learnId], [userId], [cardId], [studySetId]) VALUES (1075, 1, 4021, 2011)
INSERT [dbo].[LearnCards] ([learnId], [userId], [cardId], [studySetId]) VALUES (1076, 1, 4022, 2011)
INSERT [dbo].[LearnCards] ([learnId], [userId], [cardId], [studySetId]) VALUES (1077, 1, 4023, 2011)
INSERT [dbo].[LearnCards] ([learnId], [userId], [cardId], [studySetId]) VALUES (1078, 1, 4024, 2011)
INSERT [dbo].[LearnCards] ([learnId], [userId], [cardId], [studySetId]) VALUES (1079, 1, 4020, 2011)
INSERT [dbo].[LearnCards] ([learnId], [userId], [cardId], [studySetId]) VALUES (1080, 1, 4025, 2011)
INSERT [dbo].[LearnCards] ([learnId], [userId], [cardId], [studySetId]) VALUES (1082, 1, 4026, 2011)
INSERT [dbo].[LearnCards] ([learnId], [userId], [cardId], [studySetId]) VALUES (1083, 1, 4027, 2011)
SET IDENTITY_INSERT [dbo].[LearnCards] OFF
GO
SET IDENTITY_INSERT [dbo].[ListFolder] ON 

INSERT [dbo].[ListFolder] ([studySetId], [folderId], [ListFolderId]) VALUES (2011, 1005, 1039)
INSERT [dbo].[ListFolder] ([studySetId], [folderId], [ListFolderId]) VALUES (2012, 1005, 1040)
INSERT [dbo].[ListFolder] ([studySetId], [folderId], [ListFolderId]) VALUES (2013, 1006, 1041)
INSERT [dbo].[ListFolder] ([studySetId], [folderId], [ListFolderId]) VALUES (2014, 1006, 1042)
INSERT [dbo].[ListFolder] ([studySetId], [folderId], [ListFolderId]) VALUES (2012, 2002, 2032)
INSERT [dbo].[ListFolder] ([studySetId], [folderId], [ListFolderId]) VALUES (2011, 1003, 2033)
INSERT [dbo].[ListFolder] ([studySetId], [folderId], [ListFolderId]) VALUES (2011, 2002, 2036)
SET IDENTITY_INSERT [dbo].[ListFolder] OFF
GO
SET IDENTITY_INSERT [dbo].[ListStudySet] ON 

INSERT [dbo].[ListStudySet] ([classId], [studySetId], [ListStudySetId]) VALUES (1002, 2011, 1)
INSERT [dbo].[ListStudySet] ([classId], [studySetId], [ListStudySetId]) VALUES (3, 2011, 2)
INSERT [dbo].[ListStudySet] ([classId], [studySetId], [ListStudySetId]) VALUES (2, 2011, 3)
SET IDENTITY_INSERT [dbo].[ListStudySet] OFF
GO
SET IDENTITY_INSERT [dbo].[Page] ON 

INSERT [dbo].[Page] ([PageID], [PageName], [ChapterID]) VALUES (1, N'Trang 11: Practice Exercises  1.1', 1)
INSERT [dbo].[Page] ([PageID], [PageName], [ChapterID]) VALUES (2, N'Trang 19: Practice Exercises 1.2 ', 1)
INSERT [dbo].[Page] ([PageID], [PageName], [ChapterID]) VALUES (3, N'Trang 20: Practice Exercises 1.3', 1)
INSERT [dbo].[Page] ([PageID], [PageName], [ChapterID]) VALUES (7, N'', 25)
SET IDENTITY_INSERT [dbo].[Page] OFF
GO
SET IDENTITY_INSERT [dbo].[StudySet] ON 

INSERT [dbo].[StudySet] ([studySetId], [title], [description], [isShare], [userId]) VALUES (2011, N'English 1', N'weghawg', 1, 1)
INSERT [dbo].[StudySet] ([studySetId], [title], [description], [isShare], [userId]) VALUES (2012, N'English 2', N'', 1, 1)
INSERT [dbo].[StudySet] ([studySetId], [title], [description], [isShare], [userId]) VALUES (2013, N'Japanese', N'', 1, 1)
INSERT [dbo].[StudySet] ([studySetId], [title], [description], [isShare], [userId]) VALUES (2014, N'Japanese2', N'', 1, 1)
INSERT [dbo].[StudySet] ([studySetId], [title], [description], [isShare], [userId]) VALUES (3021, N'test isShare', N'aegg', 0, 2)
SET IDENTITY_INSERT [dbo].[StudySet] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([name], [password], [gmail], [isActive], [avatar], [userId], [language]) VALUES (N'vubui123', N'loner123', N'vubui6667@gmail.com', 1, N'', 1, N'')
INSERT [dbo].[User] ([name], [password], [gmail], [isActive], [avatar], [userId], [language]) VALUES (N'vubui2509', N'loner123', N'vbgaming67@gmail.com', 1, N'', 2, N'')
INSERT [dbo].[User] ([name], [password], [gmail], [isActive], [avatar], [userId], [language]) VALUES (N'vubuiii', N'loner123', N'test@gmail.com', 0, N'', 3, N'')
INSERT [dbo].[User] ([name], [password], [gmail], [isActive], [avatar], [userId], [language]) VALUES (N'testacc', N'loner123', N'vietanhtranhuu02@gmail.com', 0, N'', 4, N'')
SET IDENTITY_INSERT [dbo].[User] OFF
GO
ALTER TABLE [dbo].[Class] ADD  CONSTRAINT [DF_Class_isInvite]  DEFAULT ((0)) FOR [isInvite]
GO
ALTER TABLE [dbo].[Class] ADD  CONSTRAINT [DF_Class_isEdit]  DEFAULT ((0)) FOR [isEdit]
GO
ALTER TABLE [dbo].[Folder] ADD  CONSTRAINT [DF_Folder_isShare]  DEFAULT ((1)) FOR [isShare]
GO
ALTER TABLE [dbo].[StudySet] ADD  CONSTRAINT [DF_StudySet_isShare]  DEFAULT ((1)) FOR [isShare]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_isActive]  DEFAULT ((0)) FOR [isActive]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_language]  DEFAULT (N'VI') FOR [language]
GO
ALTER TABLE [dbo].[Answers]  WITH CHECK ADD  CONSTRAINT [FK_Answers_Exercises] FOREIGN KEY([ExID])
REFERENCES [dbo].[Exercises] ([ExID])
GO
ALTER TABLE [dbo].[Answers] CHECK CONSTRAINT [FK_Answers_Exercises]
GO
ALTER TABLE [dbo].[Book]  WITH CHECK ADD  CONSTRAINT [FK_Book_Category] FOREIGN KEY([CateID])
REFERENCES [dbo].[Category] ([CateID])
GO
ALTER TABLE [dbo].[Book] CHECK CONSTRAINT [FK_Book_Category]
GO
ALTER TABLE [dbo].[Card]  WITH CHECK ADD  CONSTRAINT [FK_Card_StudySet] FOREIGN KEY([studySetId])
REFERENCES [dbo].[StudySet] ([studySetId])
GO
ALTER TABLE [dbo].[Card] CHECK CONSTRAINT [FK_Card_StudySet]
GO
ALTER TABLE [dbo].[Chapter]  WITH CHECK ADD  CONSTRAINT [FK_Chapter_Book] FOREIGN KEY([ISBN])
REFERENCES [dbo].[Book] ([ISBN])
GO
ALTER TABLE [dbo].[Chapter] CHECK CONSTRAINT [FK_Chapter_Book]
GO
ALTER TABLE [dbo].[Class]  WITH CHECK ADD  CONSTRAINT [FK_Class_User] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([userId])
GO
ALTER TABLE [dbo].[Class] CHECK CONSTRAINT [FK_Class_User]
GO
ALTER TABLE [dbo].[Exercises]  WITH CHECK ADD  CONSTRAINT [FK_Exercises_Page] FOREIGN KEY([PageID])
REFERENCES [dbo].[Page] ([PageID])
GO
ALTER TABLE [dbo].[Exercises] CHECK CONSTRAINT [FK_Exercises_Page]
GO
ALTER TABLE [dbo].[FlashCards]  WITH CHECK ADD  CONSTRAINT [FK_FlashCards_Card] FOREIGN KEY([cardId])
REFERENCES [dbo].[Card] ([cardId])
GO
ALTER TABLE [dbo].[FlashCards] CHECK CONSTRAINT [FK_FlashCards_Card]
GO
ALTER TABLE [dbo].[FlashCards]  WITH CHECK ADD  CONSTRAINT [FK_FlashCards_StudySet] FOREIGN KEY([studySetId])
REFERENCES [dbo].[StudySet] ([studySetId])
GO
ALTER TABLE [dbo].[FlashCards] CHECK CONSTRAINT [FK_FlashCards_StudySet]
GO
ALTER TABLE [dbo].[FlashCards]  WITH CHECK ADD  CONSTRAINT [FK_FlashCards_User] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([userId])
GO
ALTER TABLE [dbo].[FlashCards] CHECK CONSTRAINT [FK_FlashCards_User]
GO
ALTER TABLE [dbo].[Folder]  WITH CHECK ADD  CONSTRAINT [FK_Folder_User] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([userId])
GO
ALTER TABLE [dbo].[Folder] CHECK CONSTRAINT [FK_Folder_User]
GO
ALTER TABLE [dbo].[LearnCards]  WITH CHECK ADD  CONSTRAINT [FK_LearnCards_Card] FOREIGN KEY([cardId])
REFERENCES [dbo].[Card] ([cardId])
GO
ALTER TABLE [dbo].[LearnCards] CHECK CONSTRAINT [FK_LearnCards_Card]
GO
ALTER TABLE [dbo].[LearnCards]  WITH CHECK ADD  CONSTRAINT [FK_LearnCards_User] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([userId])
GO
ALTER TABLE [dbo].[LearnCards] CHECK CONSTRAINT [FK_LearnCards_User]
GO
ALTER TABLE [dbo].[ListClass]  WITH CHECK ADD  CONSTRAINT [FK_ListClass_Class] FOREIGN KEY([classId])
REFERENCES [dbo].[Class] ([classId])
GO
ALTER TABLE [dbo].[ListClass] CHECK CONSTRAINT [FK_ListClass_Class]
GO
ALTER TABLE [dbo].[ListClass]  WITH CHECK ADD  CONSTRAINT [FK_ListClass_Folder] FOREIGN KEY([folderId])
REFERENCES [dbo].[Folder] ([folderId])
GO
ALTER TABLE [dbo].[ListClass] CHECK CONSTRAINT [FK_ListClass_Folder]
GO
ALTER TABLE [dbo].[ListEditId]  WITH CHECK ADD  CONSTRAINT [FK_ListEditId_StudySet] FOREIGN KEY([studySetId])
REFERENCES [dbo].[StudySet] ([studySetId])
GO
ALTER TABLE [dbo].[ListEditId] CHECK CONSTRAINT [FK_ListEditId_StudySet]
GO
ALTER TABLE [dbo].[ListEditId]  WITH CHECK ADD  CONSTRAINT [FK_ListEditId_User] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([userId])
GO
ALTER TABLE [dbo].[ListEditId] CHECK CONSTRAINT [FK_ListEditId_User]
GO
ALTER TABLE [dbo].[ListFolder]  WITH CHECK ADD  CONSTRAINT [FK_ListFolder_Folder] FOREIGN KEY([folderId])
REFERENCES [dbo].[Folder] ([folderId])
GO
ALTER TABLE [dbo].[ListFolder] CHECK CONSTRAINT [FK_ListFolder_Folder]
GO
ALTER TABLE [dbo].[ListFolder]  WITH CHECK ADD  CONSTRAINT [FK_ListFolder_StudySet] FOREIGN KEY([studySetId])
REFERENCES [dbo].[StudySet] ([studySetId])
GO
ALTER TABLE [dbo].[ListFolder] CHECK CONSTRAINT [FK_ListFolder_StudySet]
GO
ALTER TABLE [dbo].[ListMember]  WITH CHECK ADD  CONSTRAINT [FK_ListMember_Class] FOREIGN KEY([classId])
REFERENCES [dbo].[Class] ([classId])
GO
ALTER TABLE [dbo].[ListMember] CHECK CONSTRAINT [FK_ListMember_Class]
GO
ALTER TABLE [dbo].[ListMember]  WITH CHECK ADD  CONSTRAINT [FK_ListMember_User] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([userId])
GO
ALTER TABLE [dbo].[ListMember] CHECK CONSTRAINT [FK_ListMember_User]
GO
ALTER TABLE [dbo].[ListStudySet]  WITH CHECK ADD  CONSTRAINT [FK_ListStudySet_Class] FOREIGN KEY([classId])
REFERENCES [dbo].[Class] ([classId])
GO
ALTER TABLE [dbo].[ListStudySet] CHECK CONSTRAINT [FK_ListStudySet_Class]
GO
ALTER TABLE [dbo].[ListStudySet]  WITH CHECK ADD  CONSTRAINT [FK_ListStudySet_StudySet] FOREIGN KEY([studySetId])
REFERENCES [dbo].[StudySet] ([studySetId])
GO
ALTER TABLE [dbo].[ListStudySet] CHECK CONSTRAINT [FK_ListStudySet_StudySet]
GO
ALTER TABLE [dbo].[Page]  WITH CHECK ADD  CONSTRAINT [FK_Page_Chapter] FOREIGN KEY([ChapterID])
REFERENCES [dbo].[Chapter] ([ChapterID])
GO
ALTER TABLE [dbo].[Page] CHECK CONSTRAINT [FK_Page_Chapter]
GO
ALTER TABLE [dbo].[StudySet]  WITH CHECK ADD  CONSTRAINT [FK_StudySet_User] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([userId])
GO
ALTER TABLE [dbo].[StudySet] CHECK CONSTRAINT [FK_StudySet_User]
GO
USE [master]
GO
ALTER DATABASE [Quizlett] SET  READ_WRITE 
GO
